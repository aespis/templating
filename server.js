var express = require('express'),
    app = module.exports = express(),
    path = require('path'),
    fs = require('fs');

app.log = function(_txt, _type) {
    var now = new Date().toUTCString();
    _type = _type || 'ERROR';
    console.log('[' + _type.toUpperCase() + ':' + now + ']:',_txt);
};

//Precompile Assets
app.use(require("connect-assets")({
    gzip: false,
    compress: false
}));

app.use(function(req, res, next) {
    app.log(req.path + ' (' + req.method + ')', 'INFO');
    next();
});


//views directory for all template files
app.set('views', path.join(__dirname, 'views'));

//use ejs
app.set('view engine', 'ejs');

//set assets/images to a static directory mounted at /images
app.use('/images', express.static(__dirname + '/assets/images')); 

//set assets/fonts to a static directory mounted at /fonts
app.use('/fonts', express.static(__dirname + '/assets/fonts'));

//Catch all requests
app.get('*', function(req, res, next) {
    fs.exists(path.join(__dirname, 'views', req.path + '.ejs'), function(exists) {
        if(exists)
            return res.render(req.path.substr(1));
        else
            return res.status(404).send(null);
    });
});

app.log('Worker listening on port ' + process.env.PORT, 'Info');
var server = app.listen(process.env.PORT);